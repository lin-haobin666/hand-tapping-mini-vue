import { generate } from "../src/codegen";
import { baseParse } from "../src/parse";
import { transform } from "../src/transform";

describe("codegen", () => {
  it('生成简单的string类型的render函数', () => {
    const ast = baseParse("hi, string")
    transform(ast)
    const { code } = generate(ast)
    expect(code).toMatchSnapshot()//使用快照模式验证 （把当前的code存起来，每次运行会对比当前结果和存储的结果，不一样就报错）  
  })
})
