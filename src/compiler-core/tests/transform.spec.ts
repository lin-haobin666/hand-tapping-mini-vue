import { NodeTypes, node, textNode } from "../src/ast";
import { baseParse } from "../src/parse";
// import { TO_DISPLAY_STRING } from "../src/runtimeHelpers";
import { transform } from "../src/transform";
describe("编译原理: transform", () => {
  test("上下文状态", () => {
    const ast = baseParse(`<div>hello {{ world }}</div>`);
    /**用于处理节点的插件 */
    const plugin = (node) => {
      if (node.type === NodeTypes.TEXT) {
        node.content = node.content + 'world'
      }
    }
    transform(ast, {
      nodeTransforms: [plugin]
    });
    const nodeText = ast!.children![0]!.children![0];
    expect((nodeText as textNode).content).toBe('hello world')
    // manually store call arguments because context is mutable and shared
    // across calls
    // const calls: any[] = [];
    // const plugin = (node, context) => {
    //   calls.push([node, { ...context }]);
    // };

    // transform(ast, {
    //   nodeTransforms: [plugin],
    // });

    // const div = ast.children[0];
    // expect(calls.length).toBe(4);
    // expect(calls[0]).toMatchObject([
    //   ast,
    //   {},
    //   // TODO
    //   //       {
    //   //         parent: null,
    //   //         currentNode: ast,
    //   //       },
    // ]);
    // expect(calls[1]).toMatchObject([
    //   div,
    //   {},
    //   // TODO
    //   //   {
    //   //     parent: ast,
    //   //     currentNode: div,
    //   //   },
    // ]);
    // expect(calls[2]).toMatchObject([
    //   div.children[0],
    //   {},
    //   //       {
    //   //         parent: div,
    //   //         currentNode: div.children[0],
    //   //       },
    // ]);
    // expect(calls[3]).toMatchObject([
    //   div.children[1],
    //   {},
    //   //   {
    //   //     parent: div,
    //   //     currentNode: div.children[1],
    //   //   },
    // ]);
  });

  // test("should inject toString helper for interpolations", () => {
  //   const ast = baseParse(`{{ foo }}`);
  //   transform(ast, {});
  //   expect(ast.helpers).toContain(TO_DISPLAY_STRING);
  // });
});
