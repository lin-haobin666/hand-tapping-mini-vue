// import { ElementTypes, NodeTypes } from "../src/ast";
import { ElementTypes, NodeTypes, node } from "../src/ast";
import { baseParse } from "../src/parse";


describe("parser", () => {
    describe("文本模块", () => {
        test("单个文本", () => {
            const ast = baseParse("some text");
            const text = ast.children[0];

            expect(text).toStrictEqual({
                type: NodeTypes.TEXT,
                content: "some text",
            });
        });

        //   test("simple text with invalid end tag", () => {
        //     const ast = baseParse("some text</div>");
        //     const text = ast.children[0];

        //     expect(text).toStrictEqual({
        //       type: NodeTypes.TEXT,
        //       content: "some text",
        //     });
        //   });

        //   test("text with interpolation", () => {
        //     const ast = baseParse("some {{ foo + bar }} text");
        //     const text1 = ast.children[0];
        //     const text2 = ast.children[2];

        //     // ast.children[1] 应该是 interpolation
        //     expect(text1).toStrictEqual({
        //       type: NodeTypes.TEXT,
        //       content: "some ",
        //     });
        //     expect(text2).toStrictEqual({
        //       type: NodeTypes.TEXT,
        //       content: " text",
        //     });
        //   });
    });

    describe("解析插值模块", () => {
        test("单个插值", () => {
            // 1. 看看是不是一个 {{ 开头的
            // 2. 是的话，那么就作为 插值来处理
            // 3. 获取内部 message 的内容即可
            const ast = baseParse("{{message}}");
            const interpolation = ast.children[0];

            expect(interpolation).toStrictEqual({
                type: NodeTypes.INTERPOLATION,
                content: {
                    type: NodeTypes.SIMPLE_EXPRESSION,
                    content: `message`,
                },
            });
        });
    });

    describe("标签模块", () => {//解析Element模块
        test("单个标签包含文本", () => {
            const ast = baseParse("<div>hello</div>");
            const element = ast.children[0];

            expect(element).toStrictEqual({
                type: NodeTypes.ELEMENT,
                tag: "div",
                tagType: ElementTypes.ELEMENT,
                children: [
                    {
                        type: NodeTypes.TEXT,
                        content: "hello",
                    },
                ],
            });
        });
        test("标签包含插值", () => {
            const ast = baseParse("<div>{{ msg }}</div>");
            const element = ast.children[0];
            expect(element).toStrictEqual({
                type: NodeTypes.ELEMENT,
                tag: "div",
                tagType: ElementTypes.ELEMENT,
                children: [
                    {
                        type: NodeTypes.INTERPOLATION,
                        content: {
                            type: NodeTypes.SIMPLE_EXPRESSION,
                            content: `msg`,
                        },
                    },
                ],
            });
        });
        test("测试联合三种类型：插值 文本 标签", () => {
            const ast = baseParse("<div>hi,{{ msg }}</div>");
            const element = ast.children[0];

            expect(element).toStrictEqual({
                type: NodeTypes.ELEMENT,
                tag: "div",
                tagType: ElementTypes.ELEMENT,
                children: [
                    {
                        type: NodeTypes.TEXT,
                        content: "hi,",
                    },
                    {
                        type: NodeTypes.INTERPOLATION,
                        content: {
                            type: NodeTypes.SIMPLE_EXPRESSION,
                            content: "msg",
                        },
                    },
                ],
            });
        });
        test("测试联合三种类型，并且有嵌套的标签", () => {
            const ast = baseParse("<div><p>hi,</p>{{ msg }}</div>");
            const element = ast.children[0];

            expect(element).toStrictEqual<node>({
                type: NodeTypes.ELEMENT,
                tag: "div",
                tagType: ElementTypes.ELEMENT,
                children: [
                    {
                        type: NodeTypes.ELEMENT,
                        tag: 'p',
                        tagType: ElementTypes.ELEMENT,
                        children: [
                            {
                                type: NodeTypes.TEXT,
                                content: "hi,",
                            },
                        ]
                    },
                    {
                        type: NodeTypes.INTERPOLATION,
                        content: {
                            type: NodeTypes.SIMPLE_EXPRESSION,
                            content: "msg",
                        },
                    },
                ],
            });
        });
        test("测试是否缺少结束标签", () => {
            expect(() => {
                baseParse("<div><span></div>");
            }).toThrow("缺失结束标签：span");
        });
    });
});