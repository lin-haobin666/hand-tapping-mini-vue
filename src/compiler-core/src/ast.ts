/**节点的类型枚举 */
export const enum NodeTypes {
    /**根节点 */
    ROOT,
    /**文本节点 */
    TEXT,
    /**双花插值 {{ }}   */
    INTERPOLATION,
    /**表达式 */
    SIMPLE_EXPRESSION,
    /**元素节点 */
    ELEMENT,
    /**未知 */
    COMPOUND_EXPRESSION
}
/**Element的类型枚举 */
export const enum ElementTypes {
    /**element元素 */
    ELEMENT
}






/**节点 联合类型*/
export type node = elementNode | interpolationNode | textNode | baseNode
/**基础节点 */
interface baseNode {
    /**节点类型 */
    type: NodeTypes,
    /**孩子们（内容） */
    children?: node[]
}
/**根节点 */
export interface root extends baseNode {
    /**根节点的类型 */
    type: NodeTypes.ROOT
    /**孩子们 */
    children: node[],
    /**给codegen专用的入口节点 （而不是根节点） 在transform函数后赋值 */
    codegenNode?: node
}
/**元素类型的节点 */
export interface elementNode extends baseNode {
    /**标签字符串，比如'div'*/
    tag: string,
    /**标签的类型*/
    tagType: ElementTypes
}
/**插值类型的节点 */
export interface interpolationNode extends baseNode {
    /**内容 */
    content: {
        /**插值内容的节点类型 */
        type: NodeTypes,
        /**插值内容 */
        content: string
    }
}
/**text类型的节点 */
export interface textNode extends baseNode {
    /**文本内容 */
    content: string,
}