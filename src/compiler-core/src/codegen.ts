import { node, root, textNode } from "./ast";
/**generate函数的返回值 */
interface generateRes {
    /**生成后的代码字符串 */
    code: string
}
/**generate模块的context */
interface context {
    /**生成的js代码，一步步处理 */
    code: string
    /**往本对象中的code末尾添加数据的函数 */
    push: (source: string) => void
}


/**generate模块，把ast转代码
 * @param ast ast的根节点
 */
export const generate = (ast: root): generateRes => {
    // 目标生成的字符串：
    // `return function render(_ctx, _cache, $props, $setup, $data, $options) {
    //     return "hi"
    //   }` 

    const context = createCodegenContext()
    const { push } = context
    push('return ')//先加个return（最开头的）
    /**函数的名字 */
    const functionName = "render"
    /**函数的参数 */
    const args = ['_ctx', '_cache', '$props', '$setup', '$data', '$options']
    push(`function ${functionName}(${args}){`)
    push(`return `)
    genNode(ast.codegenNode, context);
    push(`}`)

    return {
        code: context.code
    }
}
/**根据节点生成代码，直接放到context的code中
 * @param node root上的codegenNode
 * @param context 上下文 
 */
function genNode(node: root['codegenNode'], context: context) {
    context.push(`'${(node as textNode).content}'`);
}
/**创建本模块专用的context */
function createCodegenContext() {
    const context: context = {
        code: "",
        push: (source) => {
            context.code += source
        }
    }
    return context
}

