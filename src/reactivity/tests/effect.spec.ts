import { effect, stop } from "../effect"
import { reactive } from "../reactive"

describe('effect', () => {
    it('happy path', () => {
        const user = reactive({ age: 10 })
        let nextAge
        effect(() => {
            nextAge = user.age + 1
        })
        expect(nextAge).toBe(11)
        //更新
        user.age++
        expect(nextAge).toBe(12)
    })
    it('return runner when call effect', () => {//验证在调用effect的时候返回一个函数
        let foo = 10
        /**effect的返回值就是传递进去的函数 */
        const runner = effect(() => {
            foo++
            return "字符串"
        })
        expect(foo).toBe(11)
        const r = runner() //执行这个，会触发依赖
        expect(foo).toBe(12)
        expect(r).toBe("字符串")
    })
    it("scheduler", () => {//验证 effect 的 scheduler 功能
        //通过 effect 的第二个参数，给定一个 scheduler 的函数
        //当 effect 第一次执行的时候，会执行effect的fn
        //但是当响应式对象set（update）的时候，却不会执行fn了，而是去执行scheduler
        //如果执行runner的时候，会执行fn
        let dummy;
        let run: any;
        /**scheduler 函数 */
        const scheduler = jest.fn(() => {
            run = runner;
        });
        const obj = reactive({ foo: 1 });
        const runner = effect(() => {
            dummy = obj.foo;
        }, { scheduler });
        expect(scheduler).not.toHaveBeenCalled();//验证一开始 scheduler 不会被调用
        expect(dummy).toBe(1);//验证这时候的dummy是1 （意味着effect的第一个参数执行了，第二个参数没执行） 
        obj.foo++;// should be called on first trigger
        expect(scheduler).toHaveBeenCalledTimes(1);//验证 scheduler 被调用了一次 
        expect(dummy).toBe(1);// 且 dummy 的值还是 1，说明effect函数没有执行 
        run(); //这里的run可以执行，说明这时候 scheduler 函数被执行了，才会给run赋值
        expect(dummy).toBe(2);// // should have run
    });
    it("stop", () => {//验证 effect 的 stop 功能  （停止响应式，把依赖删除了）
        let dummy;
        const obj = reactive({ prop: 1 });
        const runner = effect(() => {
            dummy = obj.prop;
        });
        obj.prop = 2;
        expect(dummy).toBe(2);
        stop(runner);//在这里调用了stop，后面我们更新响应式obj的值后，会发现dummy不会跟着更新了
        // obj.prop = 3
        obj.prop++ //使用 ++ 的方式，旧版的stop方法会失败，需要优化 （原因：obj.prop++即obj.prop = obj.prop + 1。旧代码只在set里面进行了删除依赖，而get的时候仍然会收集依赖，导致白stop了。）
        expect(dummy).toBe(2);//dummy 仍然是2 
        runner();//手动调用这个runner的话就会更新
        expect(dummy).toBe(3);
    });
    it("events: onStop", () => {//测试  effect 的 onStop 功能 （在stop后的一个回调函数）
        const onStop = jest.fn();
        const runner = effect(() => { }, { onStop });
        stop(runner);
        expect(onStop).toHaveBeenCalled();
    });
})

