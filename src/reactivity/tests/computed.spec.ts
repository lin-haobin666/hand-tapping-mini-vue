import { computed } from "../computed";
import { reactive } from "../reactive";
// import {vi} from 'vitest'

describe("computed", () => {
  it("happy path", () => { //  主流程
    const value = reactive({
      foo: 1,
    });

    const getter = computed(() => {
      return value.foo;
    });

    value.foo = 2;
    expect(getter.value).toBe(2);
  });

  it("should compute lazily", () => { //缓存、懒执行
    const value = reactive({
      foo: 1,
    });
    const getter = jest.fn(() => {
      return value.foo;
    });
    const cValue = computed(getter);
    //懒执行机制
    expect(getter).not.toHaveBeenCalled(); //没使用cValue的话就不会调用
    expect(cValue.value).toBe(1); //使用之后
    expect(getter).toHaveBeenCalledTimes(1);//使用之后

    //缓存机制，如果里面的数据没变化，就不执行
    cValue.value;//触发get
    expect(getter).toHaveBeenCalledTimes(1);//仍然是一次

    //虽然getter中用到的数据发生改变了，但是如果你没使用cValue，就不会调用getter
    value.foo = 2;
    expect(getter).toHaveBeenCalledTimes(1);

    //get了才会触发getter函数
    expect(cValue.value).toBe(2);
    expect(getter).toHaveBeenCalledTimes(2);

    //缓存机制，如果里面的数据没变化，就不执行
    cValue.value;//同上面
    expect(getter).toHaveBeenCalledTimes(2);
  });
});
