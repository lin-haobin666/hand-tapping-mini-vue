/**target 目标对象的键值 */
export type key = string | symbol
/**target 目标对象 */
export type target = Record<key, any>
/**函数 */
export type fn<T = any> = () => T