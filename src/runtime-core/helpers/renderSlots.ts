import { slots } from "../index.d";
import { Fragment, createVNode } from "../vnode";

/**处理slots。
 * - 在这里的时候，slots已经只有两种情况了，字符串或vnode数组（在initSlots函数中已经把单个vnode转为数组了）
 * - 本质和h函数是一样的 
 * @param slots 所有插槽。键值对，键是插槽名，值是一个插槽函数，函数的返回值是vnode或vnode数组
 * @param name 插槽名 （具名插槽）
 * @param props 需要传递的参数
 * @returns vnode （如果传递的不是函数，就会返回undefined报错）
 */
export function renderSlots(slots: slots, name: string, props: any) {
    /**获取到要渲染的slot */
    const slot = slots[name]
    if (slot) {
        if (typeof slot === 'function') {
            //使用幽灵标签 Fragment ，只渲染子节点 （Fragment的处理在patch函数中）
            return createVNode(Fragment, {}, slot(props)) //这时候的插槽已经被 initSlots函数 转换为 ()=>vnode[]  形式了
        } else {
            console.warn('你传递的slots不是函数形式！！');
            return
        }
    }
}