//在这里把所有对外导出的文件导出，到时候会把该文件所有内容导出   
export { h } from './h'
export { renderSlots } from './helpers/renderSlots'
export { createTextVNode } from './vnode'
export { getCurrentInstance } from './component'
export { provide, inject } from './apiInject'
export { createRenderer } from './renderer'