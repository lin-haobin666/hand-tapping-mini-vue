import { target } from "../reactivity/index.d";
import { hasOwn } from "../shared/index";
import { componentInstance } from "./index.d";
/**因为可能会有很多内容会挂载在这个proxy上，比如$el $emit等，所以在这里用一个对象来放函数，简化下面的处理器代码 */
const publicPropertiesMap: Record<string, (i: componentInstance) => any> = {
    /**实现从$el中获取, this.$el.xxxx */
    $el: (i) => i.vnode.el,
    /**实现从$slots中获取插槽的vnode数据 */
    $slots: (i) => i.slots,
    /**实现使用 $props 获取props */
    $props: (i) => i.props
}
/**组件实例的proxy的处理器 */
export const PublicInstanceProxyHandlers = {
    get({ _: instance }: { _: componentInstance }, key: string) {
        const { setupState, props } = instance
        //先实现从setupState中获取，直接可以this.xxx读取setupState的xxx
        if (setupState && key in setupState) {
            return setupState[key]
        }
        //实现props获取，直接可以this.xxx读取setupState的xxx
        if (hasOwn(setupState, key)) {
            return setupState[key]
        } else if (hasOwn(props, key)) {
            return props![key]
        }
        /**从 publicPropertiesMap 取出对应key的内容 */
        const publicGetter = publicPropertiesMap[key];
        if (publicGetter) {//判断存不存在，存在就调用
            return publicGetter(instance);
        }
    }
};