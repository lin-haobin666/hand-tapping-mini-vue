import { createVNode } from "./vnode"
import type { componentInstance, container, vnodeType_component, vnode } from './index.d'

//在这里利用高阶函数，让 createApp 能够用到 render函数  （render函数在 src\runtime-core\renderer.ts 的 createRenderer 函数内部用return的方式导出）
export function createAppAPI(render: (vnode: vnode, container: any) => void) {
    /**创建APP组件  （用户直接引入的函数是在 src\runtime-dom\index.ts 中定义的，在那里调用了高阶函数导出了DOM的render的createAPP）
     * @param  rootComponent  根组件
     */
    return function createApp(rootComponent: vnodeType_component) {
        return {
            /**挂载方法 
             * @param rootContainer 根容器 
             */
            mount(rootContainer: container) {
                //先转为虚拟节点 vnode，后续所有操作都会基于虚拟节点
                const vnode = createVNode(rootComponent)
                render(vnode, rootContainer)
            }
        }
    }
}




