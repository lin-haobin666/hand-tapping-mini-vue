import { fn, key, target } from '../reactivity/index.d'
import { ShapeFlags } from '../shared/shapeFlags'
import { runner } from '../reactivity/effect'
/**容器 */
export type container = Element

/**render函数的类型 */
export type render = fn<vnode>
/**emit函数 
 * @param instance 组件实例，用户无需传递 
 * @param event 要触发的事件名
 * @param rawArgs 剩余参数
 * @returns 
 */
export type emit = (instance: componentInstance, event: string, ...rawArgs: any) => any
/**setup第二个参数，conText */
export interface setupCtx {
    /**自定义事件调用 emit */
    emit: emit
}

/**vnode的type的组件类型。其实就是一个对象，用户编写的 */
export interface vnodeType_component {
    /**组件名 */
    name?: string,
    /**用户编写的setup函数 */
    setup?: (props?: vnodeProps, { emit }: setupCtx) => target | fn
    /**render函数 */
    render?: fn
}
/**vnode中的type，如果是组件就是对象，如果是element类型就是字符串(div、span等)  Symbol类型就是特殊标签 比如Fragment */
export type vnodeType = vnodeType_component | string | Symbol
/**vnode中的children。可能是字符串（文本节点）或者vnode数组（进行patch递归）， 或者插槽孩子 */
export type vnodeChildren = (string | Array<vnode>) | rawSlots
/**虚拟节点的key值，可以用于Diff算法 */
export type vnodeKey = string | number
/**vnode中的props。 */
export type vnodeProps = target & {
    /**虚拟节点的key值，可以用于Diff算法。非必填 */
    key?: vnodeKey
}


/**原始的插槽们，键值对形式，键是插槽名，值是vnode数组或vnode  一开始值有可能是vnode，后面会转为vnode数组，才能被h渲染*/
export type rawSlots = Record<string, rawSlot>
/**原始的单个插槽 - 原始返回值内容 */
export type rawSlot_res = vnode[] | vnode
/**原始的单个插槽   一开始值有可能是 vnode[] | vnode ，后面会转为vnode数组，才能被h渲染*/
export type rawSlot = (props: target) => rawSlot_res

/**处理后的插槽们 键值对。这时候的值一定是vnode数组了 */
export type slots = Record<string, slot>
/*处理后的单个插槽 - 处理后返回值内容 */
export type slot_res = vnode[]
/**处理后的单个插槽。vnode数组 */
export type slot = (props: target) => slot_res



/**虚拟节点 */
export interface vnode {
    /**这个vnode的根容器的DOM元素 */
    el: container | null
    /**vnode的类型。如果是组件就是一个用户编写的对象，如果是标签就是string */
    type: vnodeType
    /**虚拟节点的key值，可以用于Diff算法。 */
    key: number | string | undefined
    /**标识符。有关位运算的知识可以看 src\shared\shapeFlags.ts */
    shapeFlag: ShapeFlags
    /**该虚拟节点对应的组件实例。在 mountComponent 之后就不为null了 */
    component: componentInstance | null

    /**props属性 */
    props?: vnodeProps
    /**孩子。插槽孩子(未处理)，或者普通vnode孩子 */
    children?: vnodeChildren
}

/**组件实例，一步步把非必填的数据挂载上去 */
export interface componentInstance {
    /**是否已经挂载，为false说明需要走初始化流程，为true说明以后走更新流程 */
    isMounted: boolean
    /**该组件的虚拟节点 */
    vnode: vnode,
    /**vnode中的type，放在这里方便使用 */
    type: vnodeType
    /**自定义事件调用 emit */
    emit: emit
    /**该组件的setup的返回值 */
    setupState: target
    /**该组件的props */
    props: vnodeProps
    /**该组件的插槽，处理好了的 */
    slots: slots
    /**provide-inject的数据存储地方。 */
    provides: target
    /**父组件的实例。为undefined的话说明是根组件了 */
    parent: componentInstance | undefined
    /**当前的节点树 */
    subTree: vnode
    /**下次要更新的虚拟节点 （在updateComponent中赋值） */
    next: vnode | null

    /**render函数 */
    render?: render,
    /**代理对象。在这里会把setup返回值、el等挂载在这 */
    proxy?: target
    /**setupRenderEffect中的effect返回的runner函数，可以用于组件更新 */
    update?: runner
}



/**用户自定义渲染器的参数 */
export interface createRenderer<T = container> {
    /**用户自定义 - 创建元素函数 - 默认是DOM创建
     * @param vnodeType 虚拟节点type，比如div等
     * @returns 返回创建的元素，一般是DOM，或者自定义的内容
     */
    createElement: (vnodeType: string) => T
    /**用户自定义 - 挂载属性函数 - 默认是DOM属性挂载
     * @param el 被创建出来的元素，一般是DOM，或者自定义的内容 
     * @param key 属性的键
     * @param preVal 旧值 （可能为null， 说明是初始化） 
     * @param nextVal 新值 （可能为null， 说明属性被删除了） 
     */
    patchProp: (el: T, key: string, preVal: any, nextVal: any) => void
    /**用户自定义 - 插入元素函数 - 默认是DOM插入
     * @param child 要插入的元素，一般是DOM，或者自定义的内容  
     * @param parent 被插入的父元素
     * @param anchor 锚点，用于指定插入到哪个元素前面 
     */
    insert: (child: T, parent: T, anchor: T | null) => void
    /**用户自定义 - 移除单个孩子 - 默认是DOM移除
     * @param el 要被移除的元素，一般是DOM
     */
    remove: (el: T | null) => void
    /**用户自定义 - 将元素的孩子设置为text - 默认是设置为DOM的text
     * @param el 要被设置的元素
     * @param text 元素的孩子（字符串类型） 
     */
    setElementText: (el: T, text: string) => void
}