import type { vnodeChildren, vnodeProps, vnodeType } from "./index.d";
import { createVNode } from "./vnode";
/**h函数，是对 createVNode 的一层封装
 * @param type 节点类型，element类型就是div等字符串，组件类型就是用户传入的对象
 * @param props 属性，键值对
 * @param children 孩子，可能是字符串或者vnode数组
 * @returns 虚拟节点
 */
export function h(type: vnodeType, props?: vnodeProps, children?: vnodeChildren) {
    return createVNode(type, props, children)
}   