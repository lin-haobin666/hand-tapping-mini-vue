//这里存放所有模块下都通用的工具函数
import { target } from "../reactivity/index.d"
/**继承数据。实际上就是 Object.assign 方法，会把第2~n个参数的属性赋值给第一个参数*/
export const extend = Object.assign
/**判断这个是否是对象。基于typeof方法 （数组也是对象） */
export const isObject = (val) => {
    return val !== null && typeof val === 'object'
}
/**判断值是否改变，这里使用Object.is，是因为value可能是对象
 * @param oldVal 旧值
 * @param newVal 新值
 * @returns 改变了为true，没改变为false
 */
export const hasChange = <T>(oldVal: T, newVal: T): boolean => !Object.is(oldVal, newVal)
/**判断target上是否有key这个属性。实际上调用的是 Object.prototype.hasOwnProperty.call 方法
         * @param target 目标对象
         * @param key 目标键值
         * @returns 布尔值，有true，没有false
         */
export const hasOwn = (target: target | undefined, key: string): boolean => {
    if (!target) return false
    return Object.prototype.hasOwnProperty.call(target, key)
}

//#region 处理emit的函数等
/**检测烤肉串命名法 */
const camelizeRE = /-(\w)/g;
/** 把烤肉串命名方式转换成驼峰命名方式 */
export const camelize = (str: string): string => str.replace(camelizeRE, (_, c) => (c ? c.toUpperCase() : ""));
/**  首字母大写 */
export const capitalize = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);
/** 添加 on 前缀，并且首字母大写 */
export const toHandlerKey = (str: string) => str ? `on${capitalize(str)}` : ``;
/**用来匹配 kebab-case 的情况，比如 onTest-event 可以匹配到 T，然后取到 T 在前面加一个 - 就可以，\BT 就可以匹配到 T 前面是字母的位置 */
const hyphenateRE = /\B([A-Z])/g;
/**用来匹配 kebab-case 的情况*/
export const hyphenate = (str: string) => str.replace(hyphenateRE, "-$1").toLowerCase();
//#endregion