/**虚拟节点的类型标识符 */
export const enum ShapeFlags {
  /**element 类型 （HTML标签） */
  ELEMENT = 1, // 0001
  /**有状态的组件类型 */
  STATEFUL_COMPONENT = 1 << 1, // 0010
  /**vnode.children 为 string 类型 */
  TEXT_CHILDREN = 1 << 2, // 0100
  /**vnode.children 为数组类型 */
  ARRAY_CHILDREN = 1 << 3, // 1000
  /**vnode.children 为 slots 类型 */
  SLOTS_CHILDREN = 1 << 4 // 10000
}

//原本是打算使用对象的形式，可以通过 ShapeFlags.ELEMENT 是0还是1来知道是什么类型（也可以修改类型）。但是呢，这种方法不够高效还占内存。
// 位运算的性能特别好，但是可读性差
// 四位二进制数字，每个位置代表不同的含义，可以重叠。
// 0001 -> element 类型
// 0010 -> stateful component 有状态的组件类型
// 0100 -> vnode.children 为 string 类型
// 1000 -> vnode.children 为 vnode数组 类型
//可以重叠： 比如 1010 代表  vnode.children 为 vnode数组 且本身是个 组件 类型


//位运算概念：
//  或运算 | （两个都为0才为0，否则为1）
//  与运算 & （两个都为1才为1，否则为0）
//  左移运算符 <<  （比如 1 << 1，就是把 1(0001) 整体左移一位，变成了00010，即 2(0010)）

//进行修改：用 |
// 想把 0000 改为 0001，使用或运算
// 0000 | 0001 == 0001

//进行查找：用 &
//想判断xxxx是不是0001的element类型，就&运算
// 0101 & 0001 == 0001 --> 1 --> true
// 0110 & 0001 == 0000 --> 0 --> false
// 所以 vnode.shapeFlag & ShapeFlags.STATEFUL_COMPONENT  相当于直接返回一个布尔值，可以用作if判断


