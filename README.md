# tip

写了很详细的注释，结合 ts 与 jsDoc，用鼠标悬浮在函数/变量名上就能看到注释。ctrl+鼠标点击就能索引到对应的函数位置，方便阅读

# 分为三大模块

1. reactive 响应式模块
2. runtime-core 运行逻辑模块，包含组件创建更新等
3. compiler-core 模板编译模块

# 阅读

1. reactive 的话，可以从每个模块的 tests 文件夹中的单元测试入手，来了解这个模块的功能。
2. runtime-core 的话，查看 example 文件夹 中的 html 示例，打断点查看，每个文件是检查什么功能的会写注释，没有注释的就看文件名
3. compiler-core 模块和 reactive 一样，通过单测入手
