import pkg from './package.json' assert { type: "json" };
import typescript from '@rollup/plugin-typescript'
//在这里写好配置文件，然后在package.json里面指定打包命令即可
export default {
    input: "./src/index.ts",//指定入口
    output: [ //出口
        {
            format: "cjs",//cjs，即commonJS 规范
            file: pkg.main
        },
        {
            format: "es", // esm 规范
            file: pkg.module
        }
    ],
    plugins: [//使用一些插件
        typescript()
    ]
}