// 组件更新流程
import { h, ref } from "../../lib/guide-mini-vue.esm.js";
export default {
  name: "App",
  setup() {
    const count = ref(0)
    const onClick = () => {
      count.value++
    }
    //测试更新props
    const props = ref({
      foo: "foo",
      bar: "bar"
    })
    /**更新foo的值，应该走更新流程 */
    const onChangePropsDemo1 = () => {
      props.value.foo = 'new - foo'
    }
    /**如果foo设为undefined 或 null，应该删除props的foo */
    const onChangePropsDemo2 = () => {
      props.value.foo = undefined
    }
    /**foo的值变为foo，但是bar被删掉了 */
    const onChangePropsDemo3 = () => {
      props.value = {
        foo: 'foo'
      }
    }

    return { count, onClick, props, onChangePropsDemo1, onChangePropsDemo2, onChangePropsDemo3 }
  },
  render() {
    const { count, onClick, props, onChangePropsDemo1, onChangePropsDemo2, onChangePropsDemo3 } = this  // proxy代理 - ref无需.value
    return h(
      "div",
      {
        id: "root",
        ...props
      },
      [
        h("div", {}, "count：" + count),
        h("button", { onClick }, "点击++"),
        h("button", { onClick: onChangePropsDemo1 }, "更新foo的值，应该走更新流程"),
        h("button", { onClick: onChangePropsDemo2 }, "foo设为undefined 或 null，应该删除props的foo"),
        h("button", { onClick: onChangePropsDemo3 }, "foo的值变为foo，但是bar被删掉了"),
      ])
  },
};
