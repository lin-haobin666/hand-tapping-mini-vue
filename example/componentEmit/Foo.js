import { h, ref } from '../../lib/guide-mini-vue.esm.js';
// emit函数： 在子组件调用父组件传入的自定义事件，并且传参
//注意需要支持不同的命名方式，比如 addFoo 或 add-foo 
export default {
    setup(props, { emit }) {
        const emitAdd = () => {
            emit("add", 666, 777, 888)
        }
        return { emitAdd }
    },
    render() {
        const btn = h('button', { onClick: this.emitAdd }, "emitAdd")
        const foo = h("p", {}, "foo组件")
        return h("div", {}, [foo, btn])
    },
}