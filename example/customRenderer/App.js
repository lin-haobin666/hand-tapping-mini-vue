import { h } from '../../lib/guide-mini-vue.esm.js'
export default {
    setup() {
        return {
            x: 100,
            y: 100
        }
    },
    render() {
        const { x, y } = this
        return h("rect", { x, y })
    },
}