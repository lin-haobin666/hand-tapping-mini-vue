import { createRenderer } from "../../lib/guide-mini-vue.esm.js";
import App from "./App.js";
console.log(PIXI);
//使用的是pixijs库 https://pixijs.com/ ，使用cdn在html引入了
const game = new PIXI.Application({//创建画布
    width: 500,
    height: 500
})
document.body.append(game.view)//挂载实例
/**自定义的renderer */
const renderer = createRenderer({
    createElement(type) {
        if (type === 'rect') {//创建矩形  //这里的rect在App.js 的h函数中出现了
            const rect = new PIXI.Graphics()
            rect.beginFill(0xff0000)//开始绘制，设颜色
            rect.drawRect(0, 0, 100, 100)//绘制矩形
            rect.endFill()
            return rect
        }
    },
    patchProp(el, key, value) {
        el[key] = value //挂载属性  （把APP.js中的x y设置了
    },
    insert(el, parent) {
        parent.addChild(el)
    }
})
renderer.createApp(App).mount(game.stage) //这时候挂载的根容器就是game里的