import { h, createTextVNode } from "../../lib/guide-mini-vue.esm.js";
import Foo from "./Foo.js";
//检测插槽的使用
//1. 可以传递 string、vnode、vnode数组
//2. 在render中使用this.$slots获取
//3. 可以自定义插槽渲染位置
//4. 具名插槽 - 使用 renderSlots 的第三个参数传参 （见Foo组件）
//5. 作用域插槽 （让父组件能获取到子组件内部传出去的数据）->  需要是函数形式，才能传参
export default {
  name: "App",
  setup() { return {} },

  render() {
    const app = h("div", {}, "这是App")
    //下面是三种不同插槽
    // const slot = "这是插槽1"
    // const slot = h('p', {}, "这是插槽1")
    const slot = {
      slot1: (param) => h('p', {}, "这是插槽1，参数是：" + param), //可以传参
      slot2: () => h('p', {}, "这是插槽2"),//指定名字，具名插槽
      slot3: () => [h('p', {}, "这是插槽3-1"), h('p', {}, "这是插槽3-2")],//vnode[] 类型
      slot4: () => createTextVNode("这是插槽4，我在这里传入的是纯文本vnode(渲染后没有标签包裹)"),  //Text类型
    }
    const foo = h(Foo, {}, slot)
    return h("div", {}, [app, foo]);
  },
};
