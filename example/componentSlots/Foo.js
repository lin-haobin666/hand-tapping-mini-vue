import { h, renderSlots } from "../../lib/guide-mini-vue.esm.js";
export default {
  name: "Foo",
  setup(props, context) { },
  render() {
    const foo = h("p", {}, "这里是foo")
    const param = '这是要传递出去的参数'
    return h('div', {}, [
      renderSlots(this.$slots, "slot1", param), //可以通过第二个参数，自定义不同插槽位置
      foo,
      renderSlots(this.$slots, "slot2"),
      renderSlots(this.$slots, "slot3"),
      renderSlots(this.$slots, "slot4"),
    ])//在这里放入插槽
    // return h("div", { "data-test": "child" }, [
    //   h("div", {}, "child"),
    //   // renderSlots 会返回一个 vnode
    //   // 其本质和 h 是一样的
    //   // 第三个参数给出数据
    //   renderSlots(this.$slots, "default", {
    //     age: 16,
    //   }),
    // ]);
  },
};
