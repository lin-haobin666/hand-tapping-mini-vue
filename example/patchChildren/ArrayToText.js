// 老的是 array
// 新的是 text

import { ref, h } from "../../lib/guide-mini-vue.esm.js";
/**新children，是字符串类型 */
const nextChildren = "newChildren";
/**老children，是vnode[]类型 */
const prevChildren = [h("div", {}, "A"), h("div", {}, "B")];

export default {
  name: "ArrayToText",
  setup() {
    const isChange = ref(false);
    window.isChange = isChange; //挂载在window上，方便在控制台直接修改

    return {
      isChange,
    };
  },
  render() {
    const self = this;

    return self.isChange === true
      ? h("div", {}, nextChildren)
      : h("div", {}, prevChildren);
  },
};
