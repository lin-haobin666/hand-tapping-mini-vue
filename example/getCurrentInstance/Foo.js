import { h, getCurrentInstance } from "../../lib/guide-mini-vue.esm.js";
export default {
    name: 'Foo',
    setup(props) {
        const instance = getCurrentInstance()
        console.log('Foo: ', instance);
    },
    render() {
        return h("div", {}, "Foo组件")
    },
}