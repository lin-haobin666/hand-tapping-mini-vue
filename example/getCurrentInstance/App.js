// 可以在 setup 中使用 getCurrentInstance 获取组件实例对象
import { h, getCurrentInstance } from "../../lib/guide-mini-vue.esm.js";
import Foo from './Foo.js'
export default {
  name: "App",
  render() {
    return h("div", {}, [
      h("p", {}, "CurrentInstance Demo"),
      h(Foo)
    ])
  },
  setup() {
    console.log('App: ', getCurrentInstance()) //这个方法必须在setup里才能使用！
    // return () => h("div", {}, [h("p", {}, "getCurrentInstance")]);
  },
};
