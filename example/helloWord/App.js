import { h } from '../../lib/guide-mini-vue.esm.js'
import Foo from './Foo.js'

//测试proxy代理功能
window.aaa = null //使用这个属性，来判断render时的this是什么 （在控制台输入aaa.xxx即可查看，需要.才能触发proxy的get操作，仅查看aaa是空对象）
const App = {
    name: "App",
    render() {
        window.aaa = this
        return h(
            "div",
            {
                id: 'root',
                class: 'red hard',
                onClick() {
                    console.log('点击了');
                },
                onMousedown() {
                    console.log('按下');
                }
            },
            [
                h('div', {}, `hi, ${this.msg}`),
                h(Foo, { count: 1 })
            ],
        )
    },
    setup() {
        const msg = 'mini-vue-666'
        return {
            msg
        }
    }
}
export default App