import { h } from '../../lib/guide-mini-vue.esm.js'
//这里用于测试props 
//可以通过传参的方式得到、可以在render使用this得到、props不可被修改
export default {
    setup(props) {
        console.log('设置前', props); //可以通过传参的方式得到
        props.count++
        console.log('设置后', props); //不可以被修改  
    },
    render() {
        return h("div", {}, "foo: " + this.count) //这里的count是props中的count
    },
}